#include <iostream>
#include <Windows.h>
#include <emmintrin.h>
using namespace std;
const int N = 10000;
float m[N][N];

void Generate(int size) {
	for (int i = 0; i < size; i++) {
		for (int j = 0; j < i; j++)
			m[i][j] = 0.0;
		m[i][i] = 1.0;
		for (int j = i + 1; j < size; j++)
			m[i][j] = rand();
	}
	for (int k = 0; k < size; k++)
		for (int i = k + 1; i < size; i++)
			for (int j = 0; j < size; j++)
				m[i][j] += m[k][j];
}

void Gauss_SSE(int size) {
	for (int k = 0; k < size; k++) {
		__m128 v1 = _mm_set1_ps(m[k][k]);
		int j;
		for (j = k + 1; j + 4 <= size; j += 4) {
			//A[k][j] /= A[k][k];
			__m128 va = _mm_loadu_ps(m[k] + j);
			va = _mm_div_ps(va, v1);
			_mm_storeu_ps(m[k] + j, va);
		}
		for (; j < size; j++)
			m[k][j] /= m[k][k];
		m[k][k] = 1.0;
		for (int i = k + 1; i < size; i++) {
			__m128 vaik = _mm_set1_ps(m[i][k]);
			for (j = k + 1; j + 4 <= size; j += 4) {
				//A[i][j] = A[i][j] - A[i][k] * A[k][j];
				__m128 vaij = _mm_loadu_ps(m[i] + j);
				__m128 vakj = _mm_loadu_ps(m[k] + j);
				__m128 vx = _mm_mul_ps(vaik, vakj);
				vaij = _mm_sub_ps(vaij, vx);
				_mm_storeu_ps(m[i] + j, vaij);
			}
			for (; j < size; j++)
				m[i][j] = m[i][j] - m[i][k] * m[k][j];
			m[i][k] = 0;
		}
	}
}



int main() {

	int SIZE[4] = { 10,100,1000,10000 };
	long long head, tail, freq;
	double sum_time = 0.0;
	for (int i = 0; i < 4; i++) {
		Generate(SIZE[i]);
		QueryPerformanceFrequency((LARGE_INTEGER*)&freq);
		QueryPerformanceCounter((LARGE_INTEGER*)&head);
		Gauss_SSE(SIZE[i]);
		QueryPerformanceCounter((LARGE_INTEGER*)&tail);
		sum_time += (tail - head) * 1000.0 / freq;
		cout << "size:" << SIZE[i] << endl << "The algorithm takes:" << sum_time / double(1.0) << "ms" << endl;
	}
	return 0;
}