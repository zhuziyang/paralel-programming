#include <iostream>
#include <Windows.h>
#include <emmintrin.h>
using namespace std;
const int _NUM = 10000;
float* matrix;

void Generate(float A[][_NUM], int size) {
	for (int i = 0; i < size; i++) {
		for (int j = 0; j < i; j++)
			A[i][j] = 0.0;
		A[i][i] = 1.0;
		for (int j = i + 1; j < size; j++)
			A[i][j] = rand();
	}
	for (int k = 0; k < size; k++)
		for (int i = k + 1; i < size; i++)
			for (int j = 0; j < size; j++)
				A[i][j] += A[k][j];
}

void Gauss_SSE(float A[][_NUM], int NUM) {
	for (int k = 0; k < NUM; k++) {
		__m128 v1 = _mm_set1_ps(A[k][k]);
		int j;
		for (j = k + 1; j + 4 <= NUM; j += 4) {
			//A[k][j] /= A[k][k];
			__m128 va = _mm_loadu_ps(A[k] + j);
			va = _mm_div_ps(va, v1);
			_mm_storeu_ps(A[k] + j, va);
		}
		for (; j < NUM; j++)
			A[k][j] /= A[k][k];
		A[k][k] = 1.0;
		for (int i = k + 1; i < NUM; i++) {
			__m128 vaik = _mm_set1_ps(A[i][k]);
			for (j = k + 1; j + 4 <= NUM; j += 4) {
				//A[i][j] = A[i][j] - A[i][k] * A[k][j];
				__m128 vaij = _mm_loadu_ps(A[i] + j);
				__m128 vakj = _mm_loadu_ps(A[k] + j);
				__m128 vx = _mm_mul_ps(vaik, vakj);
				vaij = _mm_sub_ps(vaij, vx);
				_mm_storeu_ps(A[i] + j, vaij);
			}
			for (; j < NUM; j++)
				A[i][j] = A[i][j] - A[i][k] * A[k][j];
			A[i][k] = 0;
		}
	}
}



int main() {

	matrix = (float*)_aligned_malloc(_NUM * _NUM * sizeof(float), 16); // 分配16字节对齐的内存空间
	float(*matrix2D)[_NUM] = (float(*)[_NUM])matrix; // 将一维数组转为二维数组

	int SIZE[4] = { 10,100,1000,10000 };

	long long head, tail, freq;
	double sum_time = 0.0;
	for (int i = 0; i < 4; i++) {
		Generate(matrix2D, SIZE[i]);
		QueryPerformanceFrequency((LARGE_INTEGER*)&freq);
		QueryPerformanceCounter((LARGE_INTEGER*)&head);;
		Gauss_SSE(matrix2D, SIZE[i]);
		QueryPerformanceCounter((LARGE_INTEGER*)&tail);
		sum_time += (tail - head) * 1000.0 / freq;
		cout << "size:" << SIZE[i] << endl << "The algorithm takes:" << sum_time / double(1.0) << "ms" << endl;
	}

	_aligned_free(matrix); // 释放内存空间
	return 0;
}
