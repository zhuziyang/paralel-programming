#include <iostream>
#include <Windows.h>
#include <immintrin.h>
using namespace std;
const int N = 10000;
float m[N][N];

void Generate(int size) {
	for (int i = 0; i < size; i++) {
		for (int j = 0; j < i; j++)
			m[i][j] = 0.0;
		m[i][i] = 1.0;
		for (int j = i + 1; j < size; j++)
			m[i][j] = rand();
	}
	for (int k = 0; k < size; k++)
		for (int i = k + 1; i < size; i++)
			for (int j = 0; j < size; j++)
				m[i][j] += m[k][j];
}

void Gauss_AVX(int size) {
	for (int k = 0; k < size; k++) {
		__m256 v1 = _mm256_set1_ps(m[k][k]);
		int j;
		for (j = k + 1; j + 8 <= size; j += 8) {
			//m[k][j] /= m[k][k];
			__m256 va = _mm256_loadu_ps(m[k] + j);
			va = _mm256_div_ps(va, v1);
			_mm256_storeu_ps(m[k] + j, va);
		}
		for (; j < size; j++)
			m[k][j] /= m[k][k];
		m[k][k] = 1.0;
		for (int i = k + 1; i < size; i++) {
			__m256 vaik = _mm256_set1_ps(m[i][k]);
			for (j = k + 1; j + 8 <= size; j += 8) {
				//m[i][j] = m[i][j] - m[i][k] * m[k][j];
				__m256 vaij = _mm256_loadu_ps(m[i] + j);
				__m256 vakj = _mm256_loadu_ps(m[k] + j);
				__m256 vx = _mm256_mul_ps(vaik, vakj);
				vaij = _mm256_sub_ps(vaij, vx);
				_mm256_storeu_ps(m[i] + j, vaij);
			}
			for (; j < size; j++)
				m[i][j] = m[i][j] - m[i][k] * m[k][j];
			m[i][k] = 0;
		}
	}
}


int main() {


	int SIZE[4] = { 10,100,1000,10000 };
	long long head, tail, freq;
	double sum_time = 0.0;
	for (int i = 0; i < 4; i++) {
		Generate(SIZE[i]);
		QueryPerformanceFrequency((LARGE_INTEGER*)&freq);
		QueryPerformanceCounter((LARGE_INTEGER*)&head);
		Gauss_AVX(SIZE[i]);
		QueryPerformanceCounter((LARGE_INTEGER*)&tail);
		sum_time += (tail - head) * 1000.0 / freq;
		cout << "size:" << SIZE[i] << endl << "The algorithm takes:" << sum_time / double(1.0) << "ms" << endl;
	}
	return 0;
}
