#include<iostream>
#include<arm_neon.h>
#include<sys/time.h>
#include<unistd.h>
using namespace std;
const int N = 10000;
float m[N][N];

void Generate(int size) {
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < i; j++)
            m[i][j] = 0.0;
        m[i][i] = 1.0;
        for (int j = i + 1; j < size; j++)
            m[i][j] = rand();
    }
    for (int k = 0; k < size; k++)
        for (int i = k + 1; i < size; i++)
            for (int j = 0; j < size; j++)
                m[i][j] += m[k][j];
}

void Gauss_Neon(int size) {
    for (int k = 0; k < size; k++) {
        float32x4_t v1 = vmovq_n_f32(m[k][k]);
        int j;
        for (j = k + 1; j + 4 <= size; j += 4) {
            float32x4_t va = vld1q_f32(m[k] + j);
            va = vdivq_f32(va, v1);
            vst1q_f32(m[k] + j, va);
        }
        for (; j < size; j++)
            m[k][j] /= m[k][k];
        m[k][k] = 1.0;
        for (int i = k + 1; i < size; i++) {
            float32x4_t vaik = vmovq_n_f32(m[i][k]);
            for (j = k + 1; j + 4 <= size; j += 4) {
                float32x4_t vakj = vld1q_f32(m[k] + j);
                float32x4_t vaij = vld1q_f32(m[i] + j);
                float32x4_t vx = vmulq_f32(vaik, vakj);
                vaij = vsubq_f32(vaij, vx);
                vst1q_f32(m[i] + j, vaij);
            }
            for (; j < size; j++)
                m[i][j] = m[i][j] - m[i][k] * m[k][j];
            m[i][k] = 0;
        }
    }
}
int main() {
    int SIZE[4] = { 10,100,1000,10000 };
    double sum_time = 0.0;
    struct timeval start;
    struct timeval end;
    for (int i = 0; i < 4; ++i)
    {
        Generate(SIZE[i]);
        gettimeofday(&start, NULL);
        Gauss_Neon(SIZE[i]);
        gettimeofday(&end, NULL);
        sum_time += 1000000 * (end.tv_sec - start.tv_sec) + end.tv_usec - start.tv_usec;
        cout << "size:" << SIZE[i] << "The algorithm takes:" << sum_time / double(1000.0) / double(10.0) << "ms" << endl;
    }
    return 0;
}
