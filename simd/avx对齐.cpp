#include <iostream>
#include <Windows.h>
#include <immintrin.h>
using namespace std;
const int N = 10000;
//最初先将矩阵声明为一维数组(即指针)
float* matrix;
void Generate(float A[][N], int size) {
	for (int i = 0; i < size; i++) {
		for (int j = 0; j < i; j++)
			A[i][j] = 0.0;
		A[i][i] = 1.0;
		for (int j = i + 1; j < size; j++)
			A[i][j] = rand();
	}
	for (int k = 0; k < size; k++)
		for (int i = k + 1; i < size; i++)
			for (int j = 0; j < size; j++)
				A[i][j] += A[k][j];
}

void Gauss_AVX(float A[][N], int size) {
	for (int k = 0; k < size; k++) {
		__m256 v1 = _mm256_set1_ps(A[k][k]);
		int j;
		for (j = k + 1; j + 8 <= size; j += 8) {
			//A[k][j] /= A[k][k];
			__m256 va = _mm256_loadu_ps(A[k] + j);
			va = _mm256_div_ps(va, v1);
			_mm256_storeu_ps(A[k] + j, va);
		}
		for (; j < size; j++)
			A[k][j] /= A[k][k];
		A[k][k] = 1.0;
		for (int i = k + 1; i < size; i++) {
			__m256 vaik = _mm256_set1_ps(A[i][k]);
			for (j = k + 1; j + 8 <= size; j += 8) {
				//A[i][j] = A[i][j] - A[i][k] * A[k][j];
				__m256 vaij = _mm256_loadu_ps(A[i] + j);
				__m256 vakj = _mm256_loadu_ps(A[k] + j);
				__m256 vx = _mm256_mul_ps(vaik, vakj);
				vaij = _mm256_sub_ps(vaij, vx);
				_mm256_storeu_ps(A[i] + j, vaij);
			}
			for (; j < size; j++)
				A[i][j] = A[i][j] - A[i][k] * A[k][j];
			A[i][k] = 0;
		}
	}
}


int main() {
	//先为分配32字节对齐的内存空间
	matrix = (float*)_aligned_malloc(N * N * sizeof(float), 32);
	//再将一维数组转为二维数组
	float(*matrix2D)[N] = (float(*)[N])matrix;

	int SIZE[4] = { 10,100,1000,10000 };

	long long head, tail, freq;
	double sum_time = 0.0;
	for (int i = 0; i < 4; i++) {
		Generate(matrix2D, SIZE[i]);
		QueryPerformanceFrequency((LARGE_INTEGER*)&freq);
		QueryPerformanceCounter((LARGE_INTEGER*)&head);;
		Gauss_AVX(matrix2D, SIZE[i]);
		QueryPerformanceCounter((LARGE_INTEGER*)&tail);
		sum_time += (tail - head) * 1000.0 / freq;
		cout << "size:" << SIZE[i] << endl << "The algorithm takes:" << sum_time / double(1.0) << "ms" << endl;
	}
	_aligned_free(matrix); // 释放内存空间
	return 0;
}