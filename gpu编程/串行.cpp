#include <stdio.h>
#include <iostream>
#include <windows.h>
#include <stdlib.h>
#include <immintrin.h>
using namespace std;

long long head, tail, freq;

double time1;

const int n = 10000;

int test_size[5] = { 256,512,1024,2000,4000 };
int test_times[5]={10,10,5,3,3};
float m[n][n];
float result[n][n];
void m_reset(int N)
{
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < i; j++)
            m[i][j] = 0;
        m[i][i] = 1.0;
        for (int j = i + 1; j < N; j++)
            m[i][j] = rand();
    }
    for (int k = 0; k < N; k++)
        for (int i = k + 1; i < N; i++)
            for (int j = 0; j < N; j++)
                m[i][j] += m[k][j];
}

void normal_LU(int N)
{
    for (int k = 0; k < N; k++)
    {
        for (int j = k + 1; j < N; j++)
        {
            m[k][j] = m[k][j] / m[k][k];

        }
        m[k][k] = 0;
        for (int i = k + 1; i < N; i++)
        {
            for (int j = k + 1; j < N; j++)
            {
                m[i][j] -= m[i][k] * m[k][j];
            }
            m[i][k] = 0;
        }

    }
}

int main()
{
    cout<<"normal"<<endl;
    for (int i = 0; i < 5; i++)
    {
        m_reset(test_size[i]);
        time1=0;
        for(int j=0;j<test_times[i];j++)
        {
            QueryPerformanceFrequency((LARGE_INTEGER*)&freq);
            QueryPerformanceCounter((LARGE_INTEGER*)&head);
            normal_LU(test_size[i]);
            QueryPerformanceCounter((LARGE_INTEGER*)&tail);
            time1+=((tail - head) * 1000.0 / freq);
        }
        cout<<"Size:"<<test_size[i]<<"  "<<time1/test_times[i]<<"ms"<<endl;
    }
    return 0;
}
