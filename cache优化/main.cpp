#include <iostream>
#include<windows.h>
#include<iomanip>
using namespace std;

int size[4]={10,100,1000,10000};
int matrix[10000][10000];
int vec[10000];
int ans[10000];
long long t[8];

int main()
{
    //初始化
    for(int i=0;i<10000;++i){
        for(int j=0;j<10000;++j){
            matrix[i][j]=i+j;
        }
    }
    for(int i=0;i<10000;++i){
        vec[i]=i*i;
    }
    for(int i=0;i<10000;++i){
        ans[i]=0;
    }

    long long head,tail,freq;
    QueryPerformanceFrequency((LARGE_INTEGER*)&freq);

    for(int i=0;i<4;++i){
        int n=size[i];
        QueryPerformanceCounter((LARGE_INTEGER*)&head);
        for(int k=0;k<n;++k){
            for(int j=0;j<n;++j){
                ans[k]+=matrix[j][k]*vec[j];
            }
        }
        QueryPerformanceCounter((LARGE_INTEGER*)&tail);
        t[2*i]=(tail-head)*1000/freq;

        QueryPerformanceCounter((LARGE_INTEGER*)&head);
        for(int j=0;j<n;++j){
            for(int i=0;i<n;++i){
                ans[i]+=matrix[j][i]*vec[j];
            }
        }
        QueryPerformanceCounter((LARGE_INTEGER*)&tail);
        t[2*i+1]=(tail-head)*1000/freq;
    }
    cout<<setw(6)<<"size"<<setw(10)<<"平凡算法"<<setw(10)<<"优化算法"<<"   单位(ms)"endl;
    for(int i=0;i<4;++i){
        cout<<setw(6)<<size[i]<<setw(10)<<t[2*i]<<setw(10)<<t[2*i+1]<<endl;
    }


    return 0;
}


