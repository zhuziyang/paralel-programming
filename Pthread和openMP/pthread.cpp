#include <iostream>
#include<sys/time.h>
#include<pthread.h>
#include<semaphore.h>
#include <xmmintrin.h> //SSE
#include <emmintrin.h> //SSE2
#include <pmmintrin.h> //SSE3
#include <tmmintrin.h> //SSSE3
#include <smmintrin.h> //SSE4.1
#include <nmmintrin.h> //SSSE4.2
#include <immintrin.h> //AVX、AVX2

using namespace std;

float A[3200][3200];
int SIZE[] = { 100,200,400,800,1600,3200 };
int NUM_THREADS[] = { 2,4,8,16 };

typedef struct {
    int k;//消去的轮次
    int t_id;//线程id
    int size;
    int num_threads;
}threadParam_t;

//信号量定义
sem_t sem_main;
sem_t sem_workerstart[16];
sem_t sem_workerend[16];

sem_t sem_leader;
sem_t sem_Division[15];
sem_t sem_Elimination[15];

pthread_barrier_t barrier_Division;
pthread_barrier_t barrier_Elimination;

//数据初始化
void init(int _size) {
    for (int i = 0; i < _size; i++) {
        for (int j = 0; j < i; j++) {
            A[i][j] = 0.0;
        }
        A[i][i] = 1.0;
        for (int j = i + 1; j < _size; j++) {
            A[i][j] = rand();
        }
    }
    for (int k = 0; k < _size; k++) {
        for (int i = k + 1; i < _size; i++) {
            for (int j = 0; j < _size; j++) {
                A[i][j] += A[k][j];
            }
        }
    }
}

//平凡算法
void normal(int _size) {
    for (int k = 0; k < _size; k++) {
        for (int j = k + 1; j < _size; j++) {
            A[k][j] = A[k][j] / A[k][k];
        }
        A[k][k] = 1.0;
        for (int i = k + 1; i < _size; i++) {
            for (int j = k + 1; j < _size; j++) {
                A[i][j] = A[i][j] - A[i][k] * A[k][j];
            }
            A[i][k] = 0;
        }
    }
}

//sse算法
void sse(int _size) {
    for (int k = 0; k < _size; ++k)
    {
        __m128 diver = _mm_load_ps1(&A[k][k]);
        int j;
        for (j = k + 1; j < _size && ((_size - j) & 3); ++j)
            A[k][j] = A[k][j] / A[k][k];
        for (; j < _size; j += 4)
        {
            __m128 divee = _mm_loadu_ps(&A[k][j]);
            divee = _mm_div_ps(divee, diver);
            _mm_storeu_ps(&A[k][j], divee);
        }
        A[k][k] = 1.0;
        for (int i = k + 1; i < _size; i += 1)
        {
            __m128 mult1 = _mm_load_ps1(&A[i][k]);
            int j;
            for (j = k + 1; j < _size && ((_size - j) & 3); ++j)
                A[i][j] = A[i][j] - A[i][k] * A[k][j];
            for (; j < _size; j += 4)
            {
                __m128 sub1 = _mm_loadu_ps(&A[i][j]);
                __m128 mult2 = _mm_loadu_ps(&A[k][j]);
                mult2 = _mm_mul_ps(mult1, mult2);
                sub1 = _mm_sub_ps(sub1, mult2);
                _mm_storeu_ps(&A[i][j], sub1);
            }
            A[i][k] = 0.0;
        }
    }
}


//线程函数定义
void* threadFunc1(void* param) {
    threadParam_t* p = (threadParam_t*)param;
    int k = p->k;//消去的轮次
    int t_id = p->t_id;//线程编号
    int size = p->size;
    int i = k + t_id + 1;//获取自己的计算任务
    for (int j = k + 1; j < size; j++) {
        A[i][j] = A[i][j] - A[i][k] * A[k][j];
    }
    A[i][k] = 0;
    pthread_exit(nullptr);
}
//动态线程版本
void pthread1(int _size) {
    for (int k = 0; k < _size; k++) {
        //主线程做除法操作
        for (int j = k + 1; j < _size; j++) {
            A[k][j] = A[k][j] / A[k][k];
        }
        A[k][k] = 1.0;

        //创建工作线程，进行消去操作
        int worker_count = _size - 1 - k;

        pthread_t* handles = new pthread_t[worker_count];
        threadParam_t* param = new threadParam_t[worker_count];

        //分配任务
        for (int t_id = 0; t_id < worker_count; t_id++) {
            param[t_id].k = k;
            param[t_id].t_id = t_id;
            param[t_id].size = _size;
        }
        //创建线程
        for (int t_id = 0; t_id < worker_count; t_id++) {
            pthread_create(handles + t_id, nullptr, threadFunc1, param + t_id);
        }
        //主线程挂起等待所有工作线程完成此轮消去工作
        for (int t_id = 0; t_id < worker_count; t_id++) {
            pthread_join(handles[t_id], nullptr);
        }
    }
}


//线程函数定义
void* threadFunc2(void* param) {
    threadParam_t* p = (threadParam_t*)param;
    int t_id = p->t_id;
    int size = p->size;
    int num_threads = p->num_threads;
    for (int k = 0; k < size; k++) {
        sem_wait(&sem_workerstart[t_id]);//阻塞，等待主线程完成除法操作
        //循环划分任务
        for (int i = k + 1; i < size; i++) {
            //消去
            for (int j = k + t_id + 1; j < size; j += num_threads) {
                A[i][j] = A[i][j] - A[i][k] * A[k][j];
            }
            A[i][k] = 0.0;
        }
        sem_post(&sem_main);//唤醒主线程
        sem_wait(&sem_workerend[t_id]);//阻塞，等待主线程唤醒进入下一轮
    }
    pthread_exit(nullptr);
}
//静态线程+信号量同步版本
void pthread2(int _size, int num_threads) {
    //初始化信号量
    sem_init(&sem_main, 0, 0);
    for (int i = 0; i < num_threads; i++) {
        sem_init(&sem_workerstart[i], 0, 0);
        sem_init(&sem_workerend[i], 0, 0);
    }
    //创建线程
    pthread_t handles[num_threads];
    threadParam_t param[num_threads];
    for (int t_id = 0; t_id < num_threads; t_id++) {
        param[t_id].t_id = t_id;
        param[t_id].size = _size;
        param[t_id].num_threads = num_threads;
        pthread_create(handles + t_id, nullptr, threadFunc2, param + t_id);
    }
    for (int k = 0; k < _size; k++) {
        //主线程做除法操作
        for (int j = k + 1; j < _size; j++) {
            A[k][j] = A[k][j] / A[k][k];
        }
        A[k][k] = 1.0;
        //开始唤醒工作线程
        for (int t_id = 0; t_id < num_threads; t_id++) {
            sem_post(&sem_workerstart[t_id]);
        }
        //主线程睡眠
        for (int t_id = 0; t_id < num_threads; t_id++) {
            sem_wait(&sem_main);
        }
        //主线程再次唤醒工作线程
        for (int t_id = 0; t_id < num_threads; t_id++) {
            sem_post(&sem_workerend[t_id]);
        }
    }
    for (int t_id = 0; t_id < num_threads; t_id++) {
        pthread_join(handles[t_id], nullptr);
    }
    sem_destroy(&sem_main);
    for (int i = 0; i < num_threads; i++) {
        sem_destroy(&sem_workerstart[i]);
        sem_destroy(&sem_workerend[i]);
    }
}


//线程定义函数
void* threadFunc3(void* param) {
    threadParam_t* p = (threadParam_t*)param;
    int t_id = p->t_id;
    int size = p->size;
    int num_threads = p->num_threads;
    for (int k = 0; k < size; ++k) {
        //t_id 为 0 的线程做除法操作，其它工作线程先等待
        //这里只采用了一个工作线程负责除法操作，同学们可以尝试采用多个工作线程完成除法操作
        //比信号量更简洁的同步方式是使用 barrier
        if (t_id == 0) {
            for (int j = k + 1; j < size; j++) {
                A[k][j] = A[k][j] / A[k][k];
            }
            A[k][k] = 1.0;
        }
        else {
            sem_wait(&sem_Division[t_id - 1]); //阻塞,等待完成除法操作
        }
        //t_id为0的线程唤醒其它工作线程,进行消去操作
        if (t_id == 0) {
            for (int i = 0; i < num_threads - 1; i++) {
                sem_post(&sem_Division[i]);
            }
        }
        //循环划分任务（同学们可以尝试多种任务划分方式）
        for (int i = k + 1 + t_id; i < size; i += num_threads) {
            //消去
            for (int j = k + 1; j < size; ++j) {
                A[i][j] = A[i][j] - A[i][k] * A[k][j];
            }
            A[i][k] = 0.0;
        }

        if (t_id == 0) {
            for (int i = 0; i < num_threads - 1; ++i) {
                sem_wait(&sem_leader); //等待其它 worker 完成消去
            }
            for (int i = 0; i < num_threads - 1; ++i) {
                sem_post(&sem_Elimination[i]); //通知其它 worker 进入下一轮
            }
        }
        else {
            sem_post(&sem_leader);//通知 leader, 已完成消去任务
            sem_wait(&sem_Elimination[t_id - 1]); //等待通知，进入下一轮
        }
    }
    pthread_exit(NULL);
}
//静态线程+信号量同步版本+三重循环全部纳入线程函数
void pthread3(int _size, int num_threads) {
    sem_init(&sem_leader, 0, 0);
    for (int i = 0; i < num_threads - 1; ++i) {
        sem_init(&sem_Division[i], 0, 0);
        sem_init(&sem_Elimination[i], 0, 0);
    }
    //创建线程
    pthread_t handles[num_threads];//创建对应的 Handle
    threadParam_t param[num_threads];//创建对应的线程数据结构
    for (int t_id = 0; t_id < num_threads; t_id++) {
        param[t_id].t_id = t_id;
        param[t_id].num_threads = num_threads;
        param[t_id].size = _size;
        pthread_create(handles + t_id, nullptr, threadFunc3, param + t_id);
    }
    for (int t_id = 0; t_id < num_threads; t_id++) {
        pthread_join(handles[t_id], nullptr);
    }
    //销毁所有信号量
    sem_destroy(&sem_leader);
    for (int i = 0; i < num_threads; i++) {
        sem_destroy(&sem_Division[i]);
        sem_destroy(&sem_Elimination[i]);
    }
}


//线程定义函数
void* threadFunc4(void* param) {
    threadParam_t* p = (threadParam_t*)param;
    int t_id = p->t_id;
    int size = p->size;
    int num_threads = p->num_threads;
    for (int k = 0; k < size; ++k) {
        //t_id 为 0 的线程做除法操作，其它工作线程先等待
        //这里只采用了一个工作线程负责除法操作，同学们可以尝试采用多个工作线程完成除法操作
        if (t_id == 0) {
            for (int j = k + 1; j < size; j++) {
                A[k][j] = A[k][j] / A[k][k];
            }
            A[k][k] = 1.0;
        }
        //第一个同步点
        pthread_barrier_wait(&barrier_Division);
        //循环划分任务（同学们可以尝试多种任务划分方式）
        for (int i = k + 1 + t_id; i < size; i += num_threads) {
            //消去
            for (int j = k + 1; j < size; ++j) {
                A[i][j] = A[i][j] - A[i][k] * A[k][j];
            }
            A[i][k] = 0.0;
        }
        //第二个同步点
        pthread_barrier_wait(&barrier_Elimination);
    }
    pthread_exit(NULL);
}
//静态线程+barrier同步
void pthread4(int _size, int num_threads) {
    //初始化 barrier
    pthread_barrier_init(&barrier_Division, nullptr, num_threads);
    pthread_barrier_init(&barrier_Elimination, nullptr, num_threads);

    //创建线程
    pthread_t handles[num_threads];// 创建对应的 Handle
    threadParam_t param[num_threads];// 创建对应的线程数据结构
    for (int t_id = 0; t_id < num_threads; t_id++) {
        param[t_id].t_id = t_id;
        param[t_id].num_threads = num_threads;
        param[t_id].size = _size;
        pthread_create(handles + t_id, nullptr, threadFunc4, param + t_id);
    }
    for (int t_id = 0; t_id < num_threads; t_id++) {
        pthread_join(handles[t_id], nullptr);
    }

    //销毁所有的 barrier
    pthread_barrier_destroy(&barrier_Division);
    pthread_barrier_destroy(&barrier_Elimination);
}


//线程定义函数
void* threadFuncSSE(void* param) {
    threadParam_t* p = (threadParam_t*)param;
    int t_id = p->t_id;
    int size = p->size;
    int num_threads = p->num_threads;
    for (int k = 0; k < size; k++) {
        sem_wait(&sem_workerstart[t_id]);//阻塞，等待主线程完成除法操作
        //循环划分任务
        for (int i = k + t_id + 1; i < size; i += num_threads) {
            float* aik = new float[4];
            *aik = *(aik + 1) = *(aik + 2) = *(aik + 3) = A[i][k];
            __m128 vaik = _mm_load_ps(aik);//将四个单精度浮点数从内存加载到向量寄存器
            delete[] aik;
            for (int j = k + 1; j + 3 < size; j += 4) {
                __m128 vaij = _mm_loadu_ps(&A[i][j]);
                __m128 vakj = _mm_loadu_ps(&A[k][j]);
                __m128 vx = _mm_mul_ps(vaik, vakj);
                vaij = _mm_sub_ps(vaij, vx);//A[i][j] = A[i][j] - A[i][k]*A[k][j];
                _mm_storeu_ps(&A[i][j], vaij);//存储到内存
            }
            for (int j = size - size % 4; j < size; j++) {
                A[i][j] = A[i][j] - A[i][k] * A[k][j];
            }//结尾几个元素串行计算
            A[i][k] = 0;
        }
        sem_post(&sem_main);//唤醒主线程
        sem_wait(&sem_workerend[t_id]);//阻塞，等待主线程唤醒进入下一轮
    }
    pthread_exit(nullptr);
}
//静态线程+信号量同步+SSE
void pthreadSSE(int _size, int num_threads) {
    //初始化信号量
    sem_init(&sem_main, 0, 0);
    for (int i = 0; i < num_threads; i++) {
        sem_init(&sem_workerstart[i], 0, 0);
        sem_init(&sem_workerend[i], 0, 0);
    }
    //创建线程
    pthread_t handles[num_threads];
    threadParam_t param[num_threads];
    for (int t_id = 0; t_id < num_threads; t_id++) {
        param[t_id].t_id = t_id;
        param[t_id].size = _size;
        param[t_id].num_threads = num_threads;
        pthread_create(handles + t_id, nullptr, threadFuncSSE, param + t_id);
    }
    for (int k = 0; k < _size; k++) {
        //主线程做除法操作
        float* akk = new float[4];
        *akk = *(akk + 1) = *(akk + 2) = *(akk + 3) = A[k][k];
        __m128 vt = _mm_load_ps(akk);//将四个单精度浮点数从内存加载到向量寄存器
        delete[] akk;
        for (int j = k + 1; j + 3 < _size; j += 4) {
            __m128 va = _mm_loadu_ps(&A[k][j]);
            va = _mm_div_ps(va, vt);//A[k][j] = A[k][j]/A[k][k];
            _mm_storeu_ps(&A[k][j], va);//储存到内存
        }
        for (int j = _size - _size % 4; j < _size; j++) {
            A[k][j] = A[k][j] / A[k][k];
        }//结尾几个元素串行计算
        A[k][k] = 1.0;
        //开始唤醒工作线程
        for (int t_id = 0; t_id < num_threads; t_id++) {
            sem_post(&sem_workerstart[t_id]);
        }
        //主线程睡眠
        for (int t_id = 0; t_id < num_threads; t_id++) {
            sem_wait(&sem_main);
        }
        //主线程再次唤醒工作线程
        for (int t_id = 0; t_id < num_threads; t_id++) {
            sem_post(&sem_workerend[t_id]);
        }
    }
    for (int t_id = 0; t_id < num_threads; t_id++) {
        pthread_join(handles[t_id], nullptr);
    }
    sem_destroy(&sem_main);
    for (int i = 0; i < num_threads; i++) {
        sem_destroy(&sem_workerstart[i]);
        sem_destroy(&sem_workerend[i]);
    }
}


int main()
{
    struct timeval h, t;
    float time = 0.0;
        //平凡算法
        cout<<"平凡算法："<<endl;
        for(int n = 0; n < 6; n++) {
            int size=SIZE[n];
            init(size);
            gettimeofday(&h, NULL);
            normal(size);
            gettimeofday(&t, NULL);
            time = 1000*(t.tv_sec - h.tv_sec) + 0.001*(t.tv_usec - h.tv_usec);
            cout<<"SIZE: "<<size<<"    "<<"TIME: "<<time<<" ms"<<endl;
        }

        //SSE
        cout<<"SSE算法："<<endl;
        for(int n = 0; n < 6; n++) {
            int size=SIZE[n];
            init(size);
            gettimeofday(&h, NULL);
            sse(size);
            gettimeofday(&t, NULL);
            time = 1000*(t.tv_sec - h.tv_sec) + 0.001*(t.tv_usec - h.tv_usec);
            cout<<"SIZE: "<<size<<"    "<<"TIME: "<<time<<" ms"<<endl;
        }

        //动态线程版本
        cout<<"动态线程版本："<<endl;
        for(int n = 0; n < 6; n++) {
            int size=SIZE[n];
            init(size);
            gettimeofday(&h, NULL);
            pthread1(size);
            gettimeofday(&t, NULL);
            time = 1000*(t.tv_sec - h.tv_sec) + 0.001*(t.tv_usec - h.tv_usec);
            cout<<"SIZE: "<<size<<"    "<<"TIME: "<<time<<" ms"<<endl;
        }

        //静态线程+信号量同步版本
        cout<<"静态线程+信号量同步版本："<<endl;
        for(int m=0;m<4;m++){
            int num_threads=NUM_THREADS[m];
            cout<<"NUM_THREADS: "<<num_threads<<":"<<endl;
            for(int n = 0; n < 6; n++) {
                int size=SIZE[n];
                init(size);
                gettimeofday(&h, NULL);
                pthread2(size,num_threads);
                gettimeofday(&t, NULL);
                time = 1000*(t.tv_sec - h.tv_sec) + 0.001*(t.tv_usec - h.tv_usec);
                cout<<"SIZE: "<<size<<"    "<<"TIME: "<<time<<" ms"<<endl;
            }
        }

        //静态线程+信号量同步+SSE
        cout<<"静态线程+信号量同步+SSE："<<endl;
        for(int m=0;m<4;m++){
            int num_threads=NUM_THREADS[m];
            cout<<"NUM_THREADS: "<<num_threads<<":"<<endl;
            for(int n = 0; n < 6; n++) {
                int size=SIZE[n];
                init(size);
                gettimeofday(&h, NULL);
                pthreadSSE(size,num_threads);
                gettimeofday(&t, NULL);
                time = 1000*(t.tv_sec - h.tv_sec) + 0.001*(t.tv_usec - h.tv_usec);
                cout<<"SIZE: "<<size<<"    "<<"TIME: "<<time<<" ms"<<endl;
            }
        }

        //静态线程+信号量同步版本+三重循环全部纳入线程函数
        cout<<"静态线程+信号量同步版本+三重循环全部纳入线程函数："<<endl;
        for(int m=0;m<4;m++){
            int num_threads=NUM_THREADS[m];
            cout<<"NUM_THREADS: "<<num_threads<<":"<<endl;
            for(int n = 0; n < 6; n++) {
                int size=SIZE[n];
                init(size);
                gettimeofday(&h, NULL);
                pthread3(size,num_threads);
                gettimeofday(&t, NULL);
                time = 1000*(t.tv_sec - h.tv_sec) + 0.001*(t.tv_usec - h.tv_usec);
                cout<<"SIZE: "<<size<<"    "<<"TIME: "<<time<<" ms"<<endl;
            }
        }

        //静态线程+barrier同步
        cout<<"静态线程+barrier同步："<<endl;
        for(int m=0;m<4;m++){
            int num_threads=NUM_THREADS[m];
            cout<<"NUM_THREADS: "<<num_threads<<":"<<endl;
            for(int n = 0; n < 6; n++) {
                int size=SIZE[n];
                init(size);
                gettimeofday(&h, NULL);
                pthread4(size,num_threads);
                gettimeofday(&t, NULL);
                time = 1000*(t.tv_sec - h.tv_sec) + 0.001*(t.tv_usec - h.tv_usec);
                cout<<"SIZE: "<<size<<"    "<<"TIME: "<<time<<" ms"<<endl;
            }
        }
        return 0;
}
