#include<iostream>
#include<sys/time.h>
#include<omp.h>
#define NUM_THREADS 2
using namespace std;
float A[3200][3200];
int SIZE[] = { 100,200,400,800,1600,3200 };

//数据初始化
void init(int _size) {
    for (int i = 0; i < _size; i++) {
        for (int j = 0; j < i; j++) {
            A[i][j] = 0.0;
        }
        A[i][i] = 1.0;
        for (int j = i + 1; j < _size; j++) {
            A[i][j] = rand();
        }
    }
    for (int k = 0; k < _size; k++) {
        for (int i = k + 1; i < _size; i++) {
            for (int j = 0; j < _size; j++) {
                A[i][j] += A[k][j];
            }
        }
    }
}

//平凡算法
void normal(int _size) {
    for (int k = 0; k < _size; k++) {
        for (int j = k + 1; j < _size; j++) {
            A[k][j] = A[k][j] / A[k][k];
        }
        A[k][k] = 1.0;
        for (int i = k + 1; i < _size; i++) {
            for (int j = k + 1; j < _size; j++) {
                A[i][j] = A[i][j] - A[i][k] * A[k][j];
            }
            A[i][k] = 0;
        }
    }
}

void om1(int _size) {
    int i, j, k;
    float tmp;
#pragma omp parallel,num_threads(NUM_THREADS),private(i, j, k, tmp),shared(A)
    for (k = 0; k < _size; k++) {
#pragma omp master
        {
            tmp = A[k][k];
            for (j = k + 1; j < _size; j++) {
                A[k][j] = A[k][j] / tmp;
            }
            A[k][k] = 1.0;
        }
#pragma omp for
        for (i = k + 1; i < _size; i++) {
            tmp = A[i][k];
            for (j = k + 1; j < _size; j++) {
                A[i][j] = A[i][j] - tmp * A[k][j];
            }
            A[i][k] = 0;
        }
    }
}

void omSSE(int _size) {
    int i, j, k;
    float tmp;
#pragma omp parallel,num_threads(NUM_THREADS),private(i, j, k, tmp),shared(A)
    for (k = 0; k < _size; k++) {
#pragma omp master
        {
            __m128 vt = _mm_set1_ps(matrix[k][k]);
            int j = k + 1;
            for (j = k + 1; j + 4 <= N; j = j + 4)
            {
                __m128 va = _mm_loadu_ps(matrix[k] + j);
                va = _mm_div_ps(va, vt);
                _mm_storeu_ps(matrix[k] + j, va);
            }
            for (; j < N; j++)
            {
                matrix[k][j] == matrix[k][j] / matrix[k][k];
            }
            matrix[k][k] = 1.0;
        }
#pragma omp for
        __m128 vaik = _mm_set1_ps(matrix[i][k]);
        int j = k + 1;
        for (j; j + 4 <= N; j = j + 4)
        {
            __m128 vakj = _mm_loadu_ps(matrix[k] + j);
            __m128 vaij = _mm_loadu_ps(matrix[i] + j);
            __m128 vx = _mm_mul_ps(vakj, vaik);
            vaij = _mm_sub_ps(vaij, vx);
            _mm_storeu_ps(matrix[i] + j, vaij);
        }
        for (j; j < N; j++)
        {
            matrix[i][j] = matrix[i][j] - matrix[k][j] * matrix[i][k];
        }
        matrix[i][k] = 0;
    }
}

//openMP线程数为NUM_THREADS的列主元消元
void om2(int _size) {
    int i, j, k;
    float tmp;
#pragma omp parallel num_threads(NUM_THREADS),private(i, j, k, tmp)
    for (k = 0; k < _size; k++) {
#pragma omp master
        {
            tmp = A[k][k];
            for (j = k + 1; j < _size; j++) {
                A[k][j] = A[k][j] / tmp;
            }
            A[k][k] = 1.0;
        }
#pragma omp for
        for (j = k + 1; j < _size; j++) {
            float tmp = A[k][j];
            for (int i = k + 1; i < _size; i++) {
                A[i][j] = A[i][j] - tmp * A[i][k];
            }
        }
        for (i = k + 1; i < _size; i++) {
            A[i][k] = 0;
        }
    }
}

int main() {
    struct timeval h, t;
    float time = 0.0;
    //平凡算法
    cout << "平凡算法：" << endl;
    for (int n = 0; n < 6; n++) {
        int size = SIZE[n];
        init(size);
        gettimeofday(&h, NULL);
        normal(size);
        gettimeofday(&t, NULL);
        time = 1000 * (t.tv_sec - h.tv_sec) + 0.001 * (t.tv_usec - h.tv_usec);
        cout << "SIZE: " << size << "    " << "TIME: " << time << " ms" << endl;
    }

    //omp1
    cout << "openMP 行主消元：" << endl;
    cout << "NUM_THREADS" << NUM_THREADS << endl;
    for (int n = 0; n < 6; n++) {
        int size = SIZE[n];
        init(size);
        gettimeofday(&h, NULL);
        om1(size);
        gettimeofday(&t, NULL);
        time = 1000 * (t.tv_sec - h.tv_sec) + 0.001 * (t.tv_usec - h.tv_usec);
        cout << "SIZE: " << size << "    " << "TIME: " << time << " ms" << endl;
    }

    //omp1+SSE
    cout << "openMP 行主消元+SSE：" << endl;
    cout << "NUM_THREADS" << NUM_THREADS << endl;
    for (int n = 0; n < 6; n++) {
        int size = SIZE[n];
        init(size);
        gettimeofday(&h, NULL);
        omSSE(size);
        gettimeofday(&t, NULL);
        time = 1000 * (t.tv_sec - h.tv_sec) + 0.001 * (t.tv_usec - h.tv_usec);
        cout << "SIZE: " << size << "    " << "TIME: " << time << " ms" << endl;
    }

    //omp2
    cout << "openMP列主消元：" << endl;
    cout << "NUM_THREADS" << NUM_THREADS << endl;
    for (int n = 0; n < 6; n++) {
        int size = SIZE[n];
        init(size);
        gettimeofday(&h, NULL);
        om2(size);
        gettimeofday(&t, NULL);
        time = 1000 * (t.tv_sec - h.tv_sec) + 0.001 * (t.tv_usec - h.tv_usec);
        cout << "SIZE: " << size << "    " << "TIME: " << time << " ms" << endl;
    }

}