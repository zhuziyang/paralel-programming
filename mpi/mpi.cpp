#include<iostream>
#include <sys/time.h>
#include<mpi.h>
using namespace std;
const int N = 4000;
const int thread_count = 4;
float M[N][N];
float A[N][N];


void Init_M()
{
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < i; j++) 
			M[i][j] = 0;
		M[i][i] = 1.0;
		for (int j = i + 1; j < N; j++)
			M[i][j] = rand() % 1000;
	}
	for (int i = 0; i < N; i++)
	{
		int k1 = rand() % N;
		int k2 = rand() % N;
		for (int j = 0; j < N; j++) {
			M[i][j] += M[0][j];
			M[k1][j] += M[k2][j];
		}
	}
}

void Init_A(int size) {
	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < size; j++) {
			A[i][j] = M[i][j];
		}
	}
}

void Normal(int size) {
	for (int k = 0; k < size; k++) {
		for (int j = k + 1; j < size; j++) {
			A[k][j] = A[k][j] / A[k][k];
		}
		A[k][k] = 1;
		for (int i = k + 1; i < size; i++) {
			for (int j = k + 1; j < size; j++) {
				A[i][j] = A[i][j] - A[i][k] * A[k][j];
			}
			A[i][k] = 0;
		}
	}
}

void LU_Block(int size, int rank, int num_proc)
{
	int block = size / num_proc;
	int remain = size % num_proc;

	int begin = rank * block;
	int end = rank != num_proc - 1 ? begin + block : begin + block + remain;

	for (int k = 0; k < size; k++){
		if (k >= begin && k < end){
			for (int j = k + 1; j < size; j++)
				A[k][j] = A[k][j] / A[k][k];
			A[k][k] = 1.0;
			for (int p = 0; p < num_proc; p++)
				if (p != rank) 
					MPI_Send(&A[k], size, MPI_FLOAT, p, 2, MPI_COMM_WORLD);
		}else{
			int cur_p = k / block;
			MPI_Recv(&A[k], size, MPI_FLOAT, cur_p, 2,
				MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		}
		for (int i = begin; i < end && i < size; i++){
			if (i >= k + 1){
				for (int j = k + 1; j < size; j++)
					A[i][j] = A[i][j] - A[i][k] * A[k][j];
				A[i][k] = 0.0;
			}
		}
	}
}

void LU_Loop(int size, int rank, int num_proc)
{
	for (int k = 0; k < size; k++)
	{
		if (int(k % num_proc) == rank)
		{
			for (int j = k + 1; j < size; j++)
				A[k][j] = A[k][j] / A[k][k];
			A[k][k] = 1.0;
			for (int p = 0; p < num_proc; p++)
				if (p != rank)
					MPI_Send(&A[k], size, MPI_FLOAT, p, 2, MPI_COMM_WORLD);
		}
		else
		{
			MPI_Recv(&A[k], size, MPI_FLOAT, int(k % num_proc), 2,
				MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		}
		for (int i = k + 1; i < size; i++)
		{
			if (int(i % num_proc) == rank)
			{
				for (int j = k + 1; j < size; j++)
					A[i][j] = A[i][j] - A[i][k] * A[k][j];
				A[i][k] = 0.0;
			}
		}
	}
}
void MPI_Block(int size)
{
	int num_proc;
	int rank;

	MPI_Comm_size(MPI_COMM_WORLD, &num_proc);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	int block = size / num_proc;
	int remain = size % num_proc;

	if (rank == 0)
	{
		for (int i = 1; i < num_proc; i++)
		{
			if (i != num_proc - 1)
			{
				for (int j = 0; j < block; j++)
					MPI_Send(&A[i * block + j], size, MPI_FLOAT, i, 0, MPI_COMM_WORLD);
			}
			else
			{
				for (int j = 0; j < block + remain; j++)
					MPI_Send(&A[i * block + j], size, MPI_FLOAT, i, 0, MPI_COMM_WORLD);
			}
		}
		LU_Block(size, rank, num_proc);
		for (int i = 1; i < num_proc; i++)
		{
			if (i != num_proc - 1)
			{
				for (int j = 0; j < block; j++)
					MPI_Recv(&A[i * block + j], size, MPI_FLOAT, i, 1,
						MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			}
			else
			{
				for (int j = 0; j < block + remain; j++)
					MPI_Recv(&A[i * block + j], size, MPI_FLOAT, i, 1,
						MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			}
		}
	}

	else
	{
		if (rank != num_proc - 1)
		{
			for (int j = 0; j < block; j++)
				MPI_Recv(&A[rank * block + j], size, MPI_FLOAT, 0, 0,
					MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		}
		else
		{
			for (int j = 0; j < block + remain; j++)
				MPI_Recv(&A[rank * block + j], size, MPI_FLOAT, 0, 0,
					MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		}
		LU_Block(size, rank, num_proc);
		if (rank != num_proc - 1)
		{
			for (int j = 0; j < block; j++)
				MPI_Send(&A[rank * block + j], size, MPI_FLOAT, 0, 1, MPI_COMM_WORLD);
		}
		else
		{
			for (int j = 0; j < block + remain; j++)
				MPI_Send(&A[rank * block + j], size, MPI_FLOAT, 0, 1, MPI_COMM_WORLD);
		}
	}
}
void LUx(int size, int rank, int num_proc)
{
	__m128 t1, t2, t3;
	int block = size / num_proc;
	int remain = size % num_proc;
	int begin = rank * block;
	int end = rank != num_proc - 1 ? begin + block : begin + block + remain;
#pragma omp parallel num_threads(thread_count),private(t1, t2, t3)
	for (int k = 0; k < size; k++)
	{
		if (k >= begin && k < end)
		{
			float temp1[4] = { A[k][k], A[k][k], A[k][k], A[k][k] };
			t1 = _mm_loadu_ps(temp1);
#pragma omp for schedule(static)
			for (int j = k + 1; j < size - 3; j += 4)
			{
				t2 = _mm_loadu_ps(A[k] + j);
				t3 = _mm_div_ps(t2, t1);
				_mm_storeu_ps(A[k] + j, t3);
			}
			for (int j = size - size % 4; j < size; j++)
			{
				A[k][j] = A[k][j] / A[k][k];
			}
			A[k][k] = 1.0;
			for (int p = rank + 1; p < num_proc; p++)
				MPI_Send(&A[k], size, MPI_FLOAT, p, 2, MPI_COMM_WORLD);
		}
		else
		{
			int cur_p = k / block;
			if (cur_p < rank)
				MPI_Recv(&A[k], size, MPI_FLOAT, cur_p, 2, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		}
		for (int i = begin; i < end && i < size; i++)
		{
			if (i >= k + 1)
			{
				float temp2[4] = { A[i][k], A[i][k], A[i][k], A[i][k] };
				t1 = _mm_loadu_ps(temp2);
#pragma omp for schedule(static)
				for (int j = k + 1; j <= size - 3; j += 4)
				{
					t2 = _mm_loadu_ps(A[i] + j);
					t3 = _mm_loadu_ps(A[k] + j);
					t3 = _mm_mul_ps(t1, t3);
					t2 = _mm_sub_ps(t2, t3);
					_mm_storeu_ps(A[i] + j, t2);
				}
				for (int j = size - size % 4; j < size; j++)
					A[i][j] = A[i][j] - A[i][k] * A[k][j];
				A[i][k] = 0;
			}
		}
	}
}


void MPIx(int size)
{
	int num_proc;
	int rank;
	MPI_Comm_size(MPI_COMM_WORLD, &num_proc);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	int block = size / num_proc;
	int remain = size % num_proc;
	if (rank == 0) {
		for (int i = 1; i < num_proc; i++) {
			if (i != num_proc - 1) {
				for (int j = 0; j < block; j++)
					MPI_Send(&A[i * block + j], size, MPI_FLOAT, i, 0, MPI_COMM_WORLD);
			}
			else {
				for (int j = 0; j < block + remain; j++)
					MPI_Send(&A[i * block + j], size, MPI_FLOAT, i, 0, MPI_COMM_WORLD);
			}
		}
		LUx(size, rank, num_proc);
		for (int i = 1; i < num_proc; i++) {
			if (i != num_proc - 1) {
				for (int j = 0; j < block; j++)
					MPI_Recv(&A[i * block + j], size, MPI_FLOAT, i, 1,
						MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			}
			else {
				for (int j = 0; j < block + remain; j++)
					MPI_Recv(&A[i * block + j], size, MPI_FLOAT, i, 1,
						MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			}
		}
	}
	else {
		if (rank != num_proc - 1) {
			for (int j = 0; j < block; j++)
				MPI_Recv(&A[rank * block + j], size, MPI_FLOAT, 0, 0,
					MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		}
		else {
			for (int j = 0; j < block + remain; j++)
				MPI_Recv(&A[rank * block + j], size, MPI_FLOAT, 0, 0,
					MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		}
		LUx(size, rank, num_proc);
		if (rank != num_proc - 1) {
			for (int j = 0; j < block; j++)
				MPI_Send(&A[rank * block + j], size, MPI_FLOAT, 0, 1, MPI_COMM_WORLD);
		}
		else {
			for (int j = 0; j < block + remain; j++)
				MPI_Send(&A[rank * block + j], size, MPI_FLOAT, 0, 1, MPI_COMM_WORLD);
		}
	}
}
void MPI_Loop(int size)
{
	int num_proc;
	int rank;

	MPI_Comm_size(MPI_COMM_WORLD, &num_proc);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	if (rank == 0)
	{
		for (int i = 0; i < size; i++)
		{
			int flag = i % num_proc;
			if (flag == rank)
				continue;
			else
				MPI_Send(&A[i], size, MPI_FLOAT, flag, 0, MPI_COMM_WORLD);
		}
		LU_Loop(size, rank, num_proc);
		for (int i = 0; i < size; i++)
		{
			int flag = i % num_proc;
			if (flag == rank)
				continue;
			else
				MPI_Recv(&A[i], size, MPI_FLOAT, flag, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		}
	}
	else
	{
		for (int i = rank; i < size; i += num_proc)
		{
			MPI_Recv(&A[i], size, MPI_FLOAT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		}
		LU_Loop(size, rank, num_proc);

		for (int i = rank; i < size; i += num_proc)
		{
			MPI_Send(&A[i], size, MPI_FLOAT, 0, 1, MPI_COMM_WORLD);
		}
	}
}

int main()
{
	Init_M();
	int size=4000;
	timeval t_start;
	timeval t_end;

	cout << "串行算法：" << endl;
	Init_A(size);
	gettimeofday(&t_start, NULL);
	Normal(size);
	gettimeofday(&t_end, NULL);
	cout << "SIZE：" << size << "    TIME：" << 1000 * (t_end.tv_sec - t_start.tv_sec) + 0.001 * (t_end.tv_usec - t_start.tv_usec) << "ms" << endl;

	MPI_Init(NULL, NULL);
	cout << "基于循环划分的MPI：" << endl;
	Init_A(size);
	gettimeofday(&t_start, NULL);
	MPI_Loop(size);
	gettimeofday(&t_end, NULL);
	cout << "SIZE：" << size << "    TIME：" << 1000 * (t_end.tv_sec - t_start.tv_sec) + 0.001 * (t_end.tv_usec - t_start.tv_usec) << "ms" << endl;
	MPI_Finalize();

	MPI_Init(NULL, NULL);
	cout << "基于块划分的MPI：" << endl;
	Init_A(size);
	gettimeofday(&t_start, NULL);
	MPI_Block(size);
	gettimeofday(&t_end, NULL);
	cout << "SIZE：" << size << "    TIME：" << 1000 * (t_end.tv_sec - t_start.tv_sec) + 0.001 * (t_end.tv_usec - t_start.tv_usec) << "ms" << endl;
	MPI_Finalize();

	MPI_Init(NULL, NULL);
	cout << "基于块划分的MPI+OpenMP+SSE：" << endl;
	Init_A(size);
	gettimeofday(&t_start, NULL);
	MPI_Block(size);
	gettimeofday(&t_end, NULL);
	cout << "SIZE：" << size << "    TIME：" << 1000 * (t_end.tv_sec - t_start.tv_sec) + 0.001 * (t_end.tv_usec - t_start.tv_usec) << "ms" << endl;
	MPI_Finalize();

	return 0;
}



