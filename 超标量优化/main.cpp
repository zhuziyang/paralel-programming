#include <iostream>
#include<windows.h>
#include<iomanip>
using namespace std;
void recursion(int n,int *a){
    if(n==1){
        return;
    }
    else{
        for(int i=0;i<n/2;++i){
            a[i]+=a[n-i-1];
        }
        n/=2;
        recursion(n,a);
    }
}
int ssize[5]={10,100,1000,10000,100000};
int a[100000];
long long head,tail,freq;
long long t[20];
int main()
{

    for(int i=0;i<100000;++i){
        a[i]=i*i;
    }
    QueryPerformanceFrequency((LARGE_INTEGER*)&freq);
    int sum=0;

    for(int s=0;s<5;++s){
        int n=ssize[s];

        QueryPerformanceCounter((LARGE_INTEGER*)&head);
        for(int i=0;i<n;++i){
            sum+=a[i];
        }
        QueryPerformanceCounter((LARGE_INTEGER*)&tail);
        t[4*s]=(tail-head)*1000000/freq;
        sum=0;

        QueryPerformanceCounter((LARGE_INTEGER*)&head);
        int sum1=0,sum2=0;
        for(int i=0;i<n;i+=2){
            sum1+=a[i];
            sum2+=a[i+1];
        }
        sum=sum1+sum2;
        QueryPerformanceCounter((LARGE_INTEGER*)&tail);
        t[4*s+1]=(tail-head)*1000000/freq;
        sum=0;

        QueryPerformanceCounter((LARGE_INTEGER*)&head);
        recursion(n,a);
        QueryPerformanceCounter((LARGE_INTEGER*)&tail);
        t[4*s+2]=(tail-head)*1000000/freq;
        sum=0;

        QueryPerformanceCounter((LARGE_INTEGER*)&head);
        for(int m=n;m>1;m/=2){
            for(int i=0;i<m/2;++i){
                a[i]=a[i*2]+a[i*2+1];
            }
        }
        QueryPerformanceCounter((LARGE_INTEGER*)&tail);
        t[4*s+3]=(tail-head)*1000000/freq;
        sum=0;
    }
    cout<<setw(6)<<"size"<<setw(10)<<"平凡算法"<<setw(10)<<"多路链式"<<setw(10)<<"递归"<<setw(10)<<"二重循环"<<"   单位(μs)"<<endl;
    for(int i=0;i<5;++i){
        cout<<setw(6)<<ssize[i]<<setw(10)<<t[4*i]<<setw(10)<<t[4*i+1]<<setw(10)<<t[4*i+2]<<setw(10)<<t[4*i+3]<<endl;
    }


    return 0;
}

